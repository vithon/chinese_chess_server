=============
CHINESE CHESS
=============

This is a web based implementation of the classic Chinese Chess.

Two players can play against each other in a browser. Viewers can view any
match.

Installation and Setup
======================

Create a virtual environment with virtualenv_, activate it, and change to
``chinesechess`` directory.

Issue this command::

    pip install pylons==0.9.6.1 webob==1.0.0 sqlalchemy webhelpers==0.6.4

Tweak the config file as appropriate, or make a copy of ``development.ini``
and modify it.

To setup application (e.g. create database tables)::

    paster setup-app development.ini

.. _virtualenv: http://pypi.python.org/pypi/virtualenv

Serving
=======

To start serving, issue this command::

    paster serve development.ini

You should change ``development.ini`` to the correct file.

Then you can access the address printed on the screen.

License
=======

The source code is released under the GNU General Public License version 2.
