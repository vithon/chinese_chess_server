import logging
import struct
import httplib
import zlib

from chinesechess.lib.base import *
from chinesechess.model import Session
from chinesechess.model import Match, MatchParameter, DrawRequest, Chat
from chinesechess.model import form
from chinesechess.model import MAX_ROW, MAX_COL

log = logging.getLogger(__name__)

def packCode(matchId, playerId):
    code = struct.pack("IB", matchId, playerId)
    crc = zlib.crc32(code, 31337)
    code = struct.pack("5si", code, crc)
    return code.encode("base64").replace('/', '~').strip()

def unpackCode(code):
    code = code.strip().replace(' ', '+').replace('~', '/').decode("base64")
    code, crc = struct.unpack("5si", code)
    if zlib.crc32(code, 31337) != crc:
        raise Exception("invalid match code")
    match, player = struct.unpack("IB", code)
    if player not in (0, 1, 2):
        raise Exception("invalid match code")
    return match, player

class MatchController(BaseController):

    def index(self):
        return render('/match_index.mako')

    def test(self):
        match, player = unpackCode("CgAAAAAAAAAB")
        match = Session.query(Match).filter_by(id=match).one()
        while len(match.moves) > 47:
            m = match.moves[-1]
            del match.moves[-1]
            Session.delete(m)
        Session.commit()
        return render('/match_test.mako')

    def create(self):
        params = form.MatchCreateForm.to_python(request.POST)
        code = params["match_code"]
        line = None
        if code:
            match_id, player = unpackCode(code)
            oldMatch = Session.query(Match).filter_by(id=match_id).one()
            oldMatch.initialize()
            step = params["match_step"]
            for move in oldMatch.moves[:step]:
                oldMatch.applyMove(move.from_row, move.from_column,
                                move.to_row, move.to_column)
            line = oldMatch.getBoardString()
            if line.count('k') + line.count('K') != 2:
                raise Exception("Please select other suitable match")
            #if oldMatch.nextPlayer != 1:
            #    raise Exception("Red always move first")
        match = Match()
        match.name = params["name"]
        match.description = params["description"]
        if line:
            match.parameters["board"] = MatchParameter("board", line)
        chat = Chat()
        match.chat = chat
        Session.add(chat)
        Session.add(match)
        Session.commit()
        c.player1 = packCode(match.id, 1)
        c.player2 = packCode(match.id, 2)
        c.viewer = packCode(match.id, 0)
        return render('/match_create.mako')

    def list(self):
        matches = Session.query(Match).all()
        c.matches = matches
        c.packCode = lambda id: packCode(id, 0)
        return render('/match_list.mako')

    def join(self, id=None):
        if id is None:
            code = request.POST.get('code', '')
        else:
            code = id
        matchId, player = unpackCode(code)
        match = Session.query(Match).filter_by(id=matchId).one()
        match.initialize()
        c.match = match
        c.code = code
        c.player = player
        c.chatCode = packCode(match.chat.id, player)
        if player == 1:
            c.chatAuthor = _("Player 1")
        elif player == 2:
            c.chatAuthor = _("Player 2")
        else:
            c.chatAuthor = _("Observer")
        return render('/match_join.mako')

    def _ajaxMove(self, id):
        matchId, player = unpackCode(id) 
        param = form.AjaxMoveForm.to_python(request.params)
        match = Session.query(Match).filter_by(id=matchId).one()
        if match.over:
            abort(httplib.PRECONDITION_FAILED, "Match over")
        match.initialize()
        match.applyMoves()
        if player != match.nextPlayer:
            abort(httplib.PRECONDITION_FAILED, "Not your turn")
        else:
            m = [param["row1"], param["col1"], param["row2"], param["col2"]]
            match.newMove(*m)
            Session.commit()
        return 'OK'

    def _ajaxUpdate(self, id):
        matchId, player = unpackCode(id) 
        param = form.AjaxUpdateForm.to_python(request.params)
        match = Session.query(Match).filter_by(id=matchId).one()
        c.index = param["index"]
        c.moves = []
        for move in match.moves[c.index : ]:
            m = [move.from_row, move.from_column, move.to_row, move.to_column]
            c.moves.append(m)
        response = render('/match_ajax_update.mako')
        if match.over:
            response += "E"
            if match.winner:
                response += str(match.winner)
            else:
                response += '3'
        return response

    def _ajaxDraw(self, id):
        matchId, player = unpackCode(id)
        if player not in (1, 2):
            abort(httplib.FORBIDDEN)
        match = Session.query(Match).filter_by(id=matchId).one()
        for req in match.draw_requests:
            if req.player == player:
                abort(httplib.PRECONDITION_FAILED, "Request already received")
        else:
            draw = DrawRequest(player)
            match.draw_requests.append(draw)
            Session.commit()
        return 'OK'

    def _ajaxResign(self, id):
        matchId, player = unpackCode(id)
        match = Session.query(Match).filter_by(id=matchId).one()
        if match.over:
            abort(httplib.PRECONDITION_FAILED, "Match over")
        if player not in (1, 2):
            abort(httplib.FORBIDDEN)
        if player == 1:
            match.winner = 2
        else:
            match.winner = 1
        Session.commit()

    def ajax(self, id):
        ajaxActions = {"move": self._ajaxMove, "update": self._ajaxUpdate,
            "draw": self._ajaxDraw, "resign": self._ajaxResign}
        param = form.AjaxForm.to_python(request.params)
        if param["action"] in ajaxActions:
            return ajaxActions[param["action"]](id)
