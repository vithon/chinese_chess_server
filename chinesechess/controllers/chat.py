import logging
import struct
import zlib
import copy

from chinesechess.lib.base import *
from chinesechess.model import Session
from chinesechess.model import Chat, ChatMessage, chat_messages_table
from chinesechess.model import form
from chinesechess.model import MAX_ROW, MAX_COL

log = logging.getLogger(__name__)

def packCode(matchId, playerId):
    code = struct.pack("IB", matchId, playerId)
    crc = zlib.crc32(code, 31337)
    code = struct.pack("5si", code, crc)
    return code.encode("base64").strip()

def unpackCode(code):
    code = code.strip().replace(' ', '+').decode("base64")
    code, crc = struct.unpack("5si", code)
    if zlib.crc32(code, 31337) != crc:
        raise Exception("invalid match code")
    match, player = struct.unpack("IB", code)
    if player not in (0, 1, 2):
        raise Exception("invalid match code")
    return match, player

class ChatController(BaseController):

    def index(self):
        return None

    def _ajaxSend(self, id):
        chatId, author = unpackCode(id) 
        param = form.ChatSendForm.to_python(request.params)
        chat = Session.query(Chat).filter_by(id=chatId).one()
        chat.messages.append(ChatMessage(author, param["message"]))
        Session.commit()
        return 'OK'

    def _ajaxUpdate(self, id):
        chatId, author = unpackCode(id) 
        param = form.ChatUpdateForm.to_python(request.params)
        messages = Session.query(ChatMessage).filter_by(chat_id=chatId).order_by(chat_messages_table.c.id.desc()).limit(10).all()
	messages.reverse()
	c.messages = []
	for m in messages:
	    msg = copy.copy(m)
	    if m.author == 1:
		msg.author = _("Player 1")
	    elif m.author == 2:
                msg.author = _("Player 2")
	    else:
		msg.author = _("Observer")
	    c.messages.append(msg)
        return render('/chat_ajax_update.mako')

    def ajax(self, id):
        ajaxActions = {"send": self._ajaxSend, "update": self._ajaxUpdate}
        param = form.AjaxForm.to_python(request.params)
        if param["action"] in ajaxActions:
            return ajaxActions[param["action"]](id)
