"""Setup the chinesechess application"""
import logging

from paste.deploy import appconfig
from pylons import config

from chinesechess.config.environment import load_environment

log = logging.getLogger(__name__)

def setup_config(command, filename, section, vars):
    """Place any commands to setup chinesechess here"""
    conf = appconfig('config:' + filename)
    load_environment(conf.global_conf, conf.local_conf)

    import chinesechess.model as model

    engine = config['pylons.g'].sa_engine
    model.metadata.create_all(bind=engine)
