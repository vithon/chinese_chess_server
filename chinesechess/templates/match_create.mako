<%inherit file="base.mako" />
${_("Please use these game codes:")}
<ul>
	<li>${_("Player 1 (Red):")} <a href="${h.url_for(controller='match', action='join', id=c.player1)}">${c.player1}</a></li>
	<li>${_("Player 2 (Black):")} <a href="${h.url_for(controller='match', action='join', id=c.player2)}">${c.player2}</a></li>
	<li>${_("Viewer:")} <a href="${h.url_for(controller='match', action='join', id=c.viewer)}">${c.viewer}</a></li>
</ul>
