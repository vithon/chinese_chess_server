<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title>Chinese Chess</title>
		${h.stylesheet_link_tag('/skins/site.css')}
		${h.stylesheet_link_tag('/skins/chat/chat.css')}
		${h.stylesheet_link_tag('/skins/rowcol/skin.css', id='skinChineseChess')}
		${h.stylesheet_link_tag('/skins/rowcol/vboard.css', id='skinBoardOrientation')}
		${h.javascript_include_tag('/js/mootools-release-1.11.js')}
		${h.javascript_include_tag('/js/chinesechess.js')}
		${h.javascript_include_tag('/js/skin.js')}
	</head>
	<body>
		<div class="content">
			${next.body()}
		</div>
		<div class="footer">
			<p>
				<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml10-blue" alt="Valid XHTML 1.0 Strict" height="31" width="88" style="border:0;width:88px;height:31px" /></a>
				<a href="http://jigsaw.w3.org/css-validator/"><img style="border:0;width:88px;height:31px" src="http://www.w3.org/Icons/valid-css-blue.png" alt="Valid CSS!" /></a>
			</p>
		</div>
	</body>
</html>
