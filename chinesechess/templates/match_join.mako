<%inherit file="base.mako" />
<%
server_url = h.url_for(controller="match", action="ajax", id=c.code)
%>
<h1>${c.match.name}</h1>
<div>${c.match.description}</div>
<div>
${_("Skin:")} <a href="javascript:changeSkin('traditional')">Traditional</a> <a href="javascript:changeSkin('marble')">Marble</a> <a href="javascript:changeSkin('rowcol')">Row Col</a>
</div>
<div><a href="${h.url_for(controller='match', action='index')}">${_("Main")}</a> <a href="${h.url_for(controller='match', action='list')}">${_("List")}</a></div>
<div>
	<button onclick="switchSide()">${_("Switch side")}</button>
	% if c.player:
	<button onclick="sendDraw('${server_url}')">${_("Draw")}</button>
	<button onclick="sendResign('${server_url}')">${_("Resign")}</button>
	% endif
</div>
<div class="chineseChess">
	<div class="boardConfiguration">
		% for row in c.match.board:
		<div>${''.join(row)}</div>
		% endfor
	</div>
</div>
${h.javascript_include_tag('/js/soundmanager2-jsmin.js')}
<script type="text/javascript">
	soundManager.debugMode = false;
	soundManager.url = '/js/soundmanager2.swf';
	loadSkin();
	initBoard(${c.player}, '${server_url}');
</script>

		${h.javascript_include_tag('/js/chat.js')}
		<div style="width: 30em; clear: both;">
			<div id="messageDisplay" style="height: 20em;"></div>
			<div id="messageInput"></div>
		</div>

		<script type="text/javascript">
			var display = document.getElementById('messageDisplay');
			var input = document.getElementById('messageInput');
			var chat = new ChatController(display, input);
			chat.start("${c.chatAuthor}", "${h.url_for(controller='chat', action='ajax', id=c.chatCode)}");
		</script>

