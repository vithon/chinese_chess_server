<%inherit file="base.mako"/>

<div><a href="${h.url_for(controller='match', action='index')}">${_("Main")}</a> <a href="${h.url_for(controller='match', action='list')}">${_("List")}</a></div>

<div class="createGame">
	${h.start_form(h.url_for(controller='match', action="create"), method="post")}
		<fieldset>
			<legend>${_('Create a new game')}</legend>
			<div><label for="name">${_("Name")}</label> ${h.text_field("name")}</div>
			<div><label for="description">${_("Description")}</label> ${h.text_area("description")}</div>
			<fieldset>
				<legend>${_("Start from this game (optional)")}</legend>
				<div><label for="match_code">${_("Match code")}</label> ${h.text_field("match_code")}</div>
				<div><label for="match_step">${_("Start from move")}</label> ${h.text_field("match_step")}</div>
			</fieldset>
			<div>${h.submit(_('Create a new game'))}</div>
		</fieldset>
	${h.end_form()}
</div>

<div class="joinGame">
	${_("Have a game code?")}
	${h.start_form(h.url_for(controller='match', action="join"), method="post")}
		<fieldset>
			<legend>${_("Join an existing game")}</legend>
			<div><label for="code">${_("Code")}</label> ${h.text_field("code")}</div>
			<div>${h.submit(_("Join a game"), "mode")}</div>
		</fieldset>
	${h.end_form()}
</div>

<div class="about">
	${h.image_tag(source="/img/board.png", alt=_("chinese chess board"))}
</div>
