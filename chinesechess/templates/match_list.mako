<%inherit file="base.mako" />
<%
from chinesechess.model import MAX_ROW
colNames = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i')
sideColors = {1: _('Red side'), 2: _('Black side')}
%>
<table border="1">
<tr><th>${_("Match")}</th><th>${_("Moves")}</th></tr>
% for match in c.matches:
<tr>
	<td>
		<div>${match.name} <a href="${h.url_for(controller='match', action='join', id=c.packCode(match.id))}">(${_("Watch")} ${c.packCode(match.id)})</a></div>
		% if match.winner:
		<div>${_("Winner:")} ${sideColors[match.winner]}</div>
		% elif match.over:
		<div>${_("Drawn")}</div>
		% endif
		<div>${match.description}</div>
	</td>
	<td>
		<div>
			% for step, move in enumerate(match.moves):
			<span>
				${colNames[move.from_column] + str(MAX_ROW - move.from_row)} ${colNames[move.to_column] + str(MAX_ROW - move.to_row)}
			</span> /
			% endfor
		</div>
	</td>
</tr>
%endfor
</table>
