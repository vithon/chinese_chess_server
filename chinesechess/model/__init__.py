from pylons import config
from sqlalchemy import Column, MetaData, Table, types
from sqlalchemy import ForeignKey
from sqlalchemy.orm import mapper, relation
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.orm.collections import column_mapped_collection

Session = scoped_session(sessionmaker(autoflush=True, autocommit=False,
                                      bind=config['pylons.g'].sa_engine))

metadata = MetaData()

chats_table = Table('chats', metadata,
	Column('id', types.Integer, primary_key=True),
	Column('junk', types.Integer)
)

chat_messages_table = Table('chat_messages', metadata,
	Column('id', types.Integer, primary_key=True),
	Column('chat_id', types.Integer, ForeignKey('chats.id')),
	Column('author', types.Integer, nullable=False),
	Column('content', types.Unicode, nullable=False)
)

matches_table = Table('matches', metadata,
	Column('id', types.Integer, primary_key=True),
	Column('name', types.Unicode(256)),
	Column('description', types.Unicode),
	Column('winner', types.SmallInteger),
	Column('chat_id', types.Integer, ForeignKey('chats.id'))
)

match_parameters_table = Table('match_parameters', metadata,
	Column('keyword', types.String(8), primary_key=True),
	Column('match_id', types.Integer, ForeignKey('matches.id'), primary_key=True),
	Column('value', types.String)
)

moves_table = Table('moves', metadata,
	Column('id', types.Integer, primary_key=True),
	Column('match_id', types.Integer, ForeignKey('matches.id')),
	Column('from_row', types.SmallInteger, nullable=False),
	Column('from_column', types.SmallInteger, nullable=False),
	Column('to_row', types.SmallInteger, nullable=False),
	Column('to_column', types.SmallInteger, nullable=False),
)

draw_requests_table = Table('draw_requests', metadata,
	Column('match_id', types.Integer, ForeignKey('matches.id'), primary_key=True),
	Column('player', types.Integer, primary_key=True),
)

DEFAULT_STARTING_BOARD = [
			['T', 'H', 'E', 'G', 'K', 'G', 'E', 'H', 'T'],
			['.', '.', '.', '.', '.', '.', '.', '.', '.'],
			['.', 'C', '.', '.', '.', '.', '.', 'C', '.'],
			['P', '.', 'P', '.', 'P', '.', 'P', '.', 'P'],
			['.', '.', '.', '.', '.', '.', '.', '.', '.'],
			['.', '.', '.', '.', '.', '.', '.', '.', '.'],
			['p', '.', 'p', '.', 'p', '.', 'p', '.', 'p'],
			['.', 'c', '.', '.', '.', '.', '.', 'c', '.'],
			['.', '.', '.', '.', '.', '.', '.', '.', '.'],
			['t', 'h', 'e', 'g', 'k', 'g', 'e', 'h', 't'],
]

MAX_ROW = 10

MAX_COL = 9

class Match(object):

	def initialize(self):
		self.nextPlayer = 1
		if "board" in self.parameters:
			self.setBoardString(self.parameters["board"].value)
		else:
			self.board = []
			for index, row in enumerate(DEFAULT_STARTING_BOARD):
				self.board.append(row[:])

	@property
	def over(self):
		return (len(self.draw_requests) == 2) or self.winner
	
	def applyMove(self, row1, col1, row2, col2):
		f = self.board[row1][col1]
		t = self.board[row2][col2]
		# TODO Check validity
		if f == '.':
			raise Exception("empty move")
		if self.nextPlayer == 1 and \
			(('A' <= f <= 'Z') or ('a' <= t <= 'z')):
			raise Exception("invalid move")
		if self.nextPlayer == 2 and \
			(('a' <= f <= 'z') or ('A' <= t <= 'Z')):
			raise Exception("invalid move")
		self.board[row2][col2] = f
		self.board[row1][col1] = '.'
		if self.nextPlayer == 1:
			self.nextPlayer = 2
		else:
			self.nextPlayer = 1
		if t == 'K':
			self.winner = 1
		elif t == 'k':
			self.winner = 2

	def newMove(self, row1, col1, row2, col2):
		if self.over:
			raise Exception("game over")
		self.applyMove(row1, col1, row2, col2)
		self.moves.append(Move(row1, col1, row2, col2))

	def applyMoves(self):
		for move in self.moves:
			self.applyMove(move.from_row, move.from_column,
					move.to_row, move.to_column)
	
	def getBoardString(self):
		lines = []
		for row in self.board:
			line = ''.join(row)
			lines.append(line)
		return '/'.join(lines)

	def setBoardString(self, s):
		lines = s.split('/')
		self.board = []
		for line in lines:
			self.board.append(list(line))

class Move(object):
	
	def __init__(self, from_row, from_col, to_row, to_col):
		self.from_row = from_row
		self.from_column = from_col
		self.to_row = to_row
		self.to_column = to_col

class MatchParameter(object):

	def __init__(self, keyword, value):
		self.keyword = keyword
		self.value = value

class DrawRequest(object):

	def __init__(self, player):
		self.player = player

class Chat(object): pass

class ChatMessage(object):

	def __init__(self, author, content):
		self.author = author
		self.content = content

mapper(Match, matches_table, properties={'moves': relation(Move), 'draw_requests': relation(DrawRequest), 'parameters': relation(MatchParameter, collection_class=column_mapped_collection(match_parameters_table.c.keyword)), 'chat': relation(Chat)})

mapper(Move, moves_table)

mapper(MatchParameter, match_parameters_table)

mapper(DrawRequest, draw_requests_table)

mapper(ChatMessage, chat_messages_table)

mapper(Chat, chats_table, properties={'messages': relation(ChatMessage)})
