import formencode
from chinesechess.model import MAX_ROW, MAX_COL
from gettext import gettext as _

class MatchCreateForm(formencode.Schema):
	
	allow_extra_fields = True
	filter_extra_fields = True
	name = formencode.validators.UnicodeString(not_empty=True)
	description = formencode.validators.UnicodeString(not_empty=True)
	match_code = formencode.validators.String(not_empty=False)
	match_step = formencode.validators.Int(not_empty=False)

class AjaxForm(formencode.Schema):
	allow_extra_fields = True
	filter_extra_fields = True

	action = formencode.validators.String(not_empty=True)

class ChatSendForm(AjaxForm):

	message = formencode.validators.UnicodeString(not_empty=True)

	def validate_python(self, value, state):
		if value["action"] != "send":
			raise formencode.Invalid(_("invalid action"), value, state)

class ChatUpdateForm(AjaxForm):

	def validate_python(self, value, state):
		if value["action"] != "update":
			raise formencode.Invalid(_("invalid action"), value, state)

class AjaxMoveForm(AjaxForm):

	row1 = formencode.validators.Int(not_empty=True)
	col1 = formencode.validators.Int(not_empty=True)
	row2 = formencode.validators.Int(not_empty=True)
	col2 = formencode.validators.Int(not_empty=True)

	def validate_python(self, value, state):
		if value["action"] != "move":
			raise formencode.Invalid(_("invalid action"), value, state)
		if (value["row1"] < 0) or (value["row2"] < 0) or \
			(value["col1"] < 0) or (value["col2"] < 0):
			raise formencode.Invalid(_("negative value"), value, state)
		if (value["row1"] >= MAX_ROW) or (value["row2"] >= MAX_ROW) or \
			(value["col1"] >= MAX_COL) or (value["col2"] >= MAX_COL):
			raise formencode.Invalid(_("over bound value"), value, state)

class AjaxUpdateForm(AjaxForm):

	index = formencode.validators.Int(not_empty=True)

	def validate_python(self, value, state):
		if value["action"] != "update":
			raise formencode.Invalid(_("invalid action"), value, state)

class AjaxDrawForm(AjaxForm):

	index = formencode.validators.Int(not_empty=True)

	def validate_python(self, value, state):
		if value["action"] != "draw":
			raise formencode.Invalid(_("invalid action"), value, state)

class AjaxResignForm(AjaxForm):

	index = formencode.validators.Int(not_empty=True)

	def validate_python(self, value, state):
		if value["action"] != "resign":
			raise formencode.Invalid(_("invalid action"), value, state)
