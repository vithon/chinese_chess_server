###
#
# Copyright 2008 Nam T. Nguyen <namn@bluemoon.com.vn>
#
# Released to the public domain.
#
# How to use:
#
# 1. Hook up a named route in your routes mapper.
#
#    map.connect('crypt_url', '',
#                conditions={'function': routescrypt.decrypt},
#                _filter=routescrypt.encrypt)
#
# 2. When you want to output crypted URL, call url_for like this
#
#    url_for('crypt_url', controller='blog', action='view', id=123)
#
# 3. The crypted URL will have this form:
#
#    http://host/app/?x=controller%3Dblog%26action%3Dview%26id%3D123
#
# 4. Modify this file to embed security token and use stronger encryption.
#
# Note: You cannot use GET parameters named 'controller' and 'action'.
#
###

import urllib
import cgi
import time
from base64 import urlsafe_b64encode, urlsafe_b64decode

def _fix(params):
	'''Fix GET parameters with, for e.g., timestamp, checksum.
	Modify this function to add your own parameters before
	they are encoded.'''

	params['__time'] = time.time()

def _check(params):
	'''Check the decoded parameters if they are valid.
	Modify this function to verify that the decoded parameters
	do contain valid, for e.g., timestamp, checksum.'''

	__time = params.pop('__time')
	if float(__time) < time.time() - 30*60:
		raise ValueError()

def _encrypt(s):
	'''Encrypt the encoded URL.
	Hook up your own encryption routine here to make it stronger.'''

	#return urlsafe_b64encode(s)
	return s

def _decrypt(s):
	'''Decrypt the URL to an encoded GET URL.
	This function must perform the reverse action of _encrypt.'''

	#return urlsafe_b64decode(s)
	return s

def encrypt(kwargs):
	_fix(kwargs)
	x = urllib.urlencode(kwargs)
	r = {}
	r['x'] = _encrypt(x)
	return r

def decrypt(environ, kwargs):
	query = environ['QUERY_STRING']
	if not query.startswith('x='):
		return False
	query = urllib.unquote_plus(query[2:])
	x = _decrypt(query)
	params = dict(cgi.parse_qsl(x, False, True))
	_check(params)
	kwargs.update(params)
	# Remove special keys from params
	for key in ('controller', 'action'):
		try:
			del params[key]
		except KeyError:
			pass
	# Put back other GET parameters in the QUERY_STRING
	environ['QUERY_STRING'] = urllib.urlencode(params)
	return True
