LOCAL_MODE=document.location.protocol=="file:";
SERVER_URL="server.html";
MAX_ROW=10;
MAX_COL=9;
EMPTY=0;
PAWN=1;
CANNON=2;
TANK=3;
HORSE=4;
ELEPHANT=5;
GUARD=6;
KING=7;
RED_SIDE=1;
BLACK_SIDE=2;
UNIT_TYPE=["empty","pawn","cannon","tank","horse","elephant","guard","king"];
function encodeUri(_1){
var _2=[];
for(key in _1){
_2.push(escape(key)+"="+escape(_1[key]));
}
return _2.join("&");
}
function generateGuid(){
var _3,i,j;
_3="";
for(j=0;j<32;j++){
if(j==8||j==12||j==16||j==20){
_3=_3+"-";
}
i=Math.floor(Math.random()*16).toString(16).toUpperCase();
_3=_3+i;
}
return _3;
}
function generateBoardId(){
return "board"+generateGuid();
}
function generatePieceId(_6,_7,_8){
return _6+"_"+_7+"_"+_8;
}
function getBoardId(_9){
return id.substring(0,_9.indexOf("_"));
}
function removeChildren(_a){
while(_a.childNodes.length){
_a.removeChild(_a.firstChild);
}
}
function generateMoves(_b){
var _c=null;
var _d=[EmptyMoveGenerator,PawnMoveGenerator,CannonMoveGenerator,TankMoveGenerator,HorseMoveGenerator,ElephantMoveGenerator,GuardMoveGenerator,KingMoveGenerator];
if(_b.type>=0&&_b.type<_d.length){
_c=new _d[_b.type](_b);
}else{
_c=new EmptyMoveGenerator(_b);
}
var _e=_c.generateMoves();
var _f=[];
while(_e.length>0){
var loc=_e.pop();
if(loc[0]>=0&&loc[0]<MAX_ROW&&loc[1]>=0&&loc[1]<MAX_COL){
var p=_b.board.board[loc[0]][loc[1]];
if(_b.canTakeOver(p)){
_f.push(loc);
}
}
}
return _f;
}
function EmptyMoveGenerator(_12){
this.generateMoves=function(){
return [];
};
}
function PawnMoveGenerator(_13){
this.board=_13.board.board;
this.piece=_13;
this.generateMoves=function(){
function crossedRiver(_14){
if(_14.side==RED_SIDE){
return _14.row<=4;
}else{
return _14.row>=5;
}
}
var _15=[];
var vr=-1;
if(this.piece.side!=RED_SIDE){
vr=1;
}
_15.push([this.piece.row+vr,this.piece.col]);
if(crossedRiver(this.piece)){
_15.push([this.piece.row,this.piece.col-1]);
_15.push([this.piece.row,this.piece.col+1]);
}
return _15;
};
}
function TankMoveGenerator(_17){
this.board=_17.board.board;
this.piece=_17;
this.velocity=[[-1,0],[1,0],[0,-1],[0,1]];
this.searchDirection=function(vr,vc){
var _1a=[];
var nr=this.piece.row;
var nc=this.piece.col;
while(true){
nr+=vr;
nc+=vc;
if(nr<0||nr>=MAX_ROW||nc<0||nc>=MAX_COL){
break;
}
_1a.push([nr,nc]);
if(this.board[nr][nc].type!=EMPTY){
if(this.board[nr][nc].side==this.piece.side){
_1a.pop();
}
break;
}
}
return _1a;
};
this.generateMoves=function(){
var _1d=[];
for(var i=0;i<this.velocity.length;i++){
var vr=this.velocity[i][0];
var vc=this.velocity[i][1];
var m=this.searchDirection(vr,vc);
_1d=_1d.concat(m);
}
return _1d;
};
}
function GuardMoveGenerator(_22){
this.piece=_22;
this.board=_22.board.board;
this.velocity=[[-1,-1],[-1,1],[1,1],[1,-1]];
this.generateMoves=function(){
function withinBound(_23,row,col){
var _26=false;
if(_23.side==RED_SIDE){
_26=(row>=7);
}else{
_26=(row<=2);
}
return _26&&(col>=3&&col<=5);
}
var _27=[];
for(var i=0;i<this.velocity.length;i++){
var vr=this.velocity[i][0];
var vc=this.velocity[i][1];
var nr=this.piece.row+vr;
var nc=this.piece.col+vc;
if(withinBound(this.piece,nr,nc)){
_27.push([nr,nc]);
}
}
return _27;
};
}
function KingMoveGenerator(_2d){
this.piece=_2d;
this.board=_2d.board.board;
this.velocity=[[-1,0],[1,0],[0,1],[0,-1]];
this.generateMoves=function(){
function withinBound(_2e,row,col){
var _31=false;
if(_2e.side==RED_SIDE){
_31=(row>=7);
}else{
_31=(row<=2);
}
return _31&&(col>=3&&col<=5);
}
var _32=[];
for(var i=0;i<this.velocity.length;i++){
var vr=this.velocity[i][0];
var vc=this.velocity[i][1];
var nr=this.piece.row+vr;
var nc=this.piece.col+vc;
if(withinBound(this.piece,nr,nc)){
_32.push([nr,nc]);
}
}
var vr=-1;
if(this.piece.side!=RED_SIDE){
vr=1;
}
for(var i=_2d.row+vr;MAX_ROW>i&&i>=0;i+=vr){
if((i==MAX_ROW||i==0)&&this.board[i][_2d.col].type==KING){
_32.push([i,_2d.col]);
break;
}
if(this.board[i][_2d.col].type!=EMPTY){
break;
}
}
return _32;
};
}
function ElephantMoveGenerator(_38){
this.piece=_38;
this.board=_38.board.board;
this.velocity=[[-2,-2],[-2,2],[2,2],[2,-2]];
this.block=[[-1,-1],[-1,1],[1,1],[1,-1]];
this.generateMoves=function(){
function withinBound(_39,row){
if(_39.side==RED_SIDE){
return row>=5;
}else{
return row<=4;
}
}
var _3b=[];
for(var i=0;i<this.velocity.length;i++){
var br=this.piece.row+this.block[i][0];
var bc=this.piece.col+this.block[i][1];
if(br<0||br>=MAX_ROW||bc<0||bc>=MAX_COL){
continue;
}
if(this.board[br][bc].type!=EMPTY){
continue;
}
var vr=this.velocity[i][0];
var vc=this.velocity[i][1];
var nr=this.piece.row+vr;
var nc=this.piece.col+vc;
if(withinBound(this.piece,nr)){
_3b.push([nr,nc]);
}
}
return _3b;
};
}
function HorseMoveGenerator(_43){
this.piece=_43;
this.board=_43.board.board;
this.velocity=[[-2,-1],[-2,1],[-1,2],[1,2],[2,1],[2,-1],[1,-2],[-1,-2]];
this.block=[[-1,0],[-1,0],[0,1],[0,1],[1,0],[1,0],[0,-1],[0,-1]];
this.generateMoves=function(){
var _44=[];
for(var i=0;i<this.velocity.length;i++){
var br=this.piece.row+this.block[i][0];
var bc=this.piece.col+this.block[i][1];
if(br<0||br>=MAX_ROW||bc<0||bc>=MAX_COL){
continue;
}
if(this.board[br][bc].type!=EMPTY){
continue;
}
var vr=this.velocity[i][0];
var vc=this.velocity[i][1];
var nr=this.piece.row+vr;
var nc=this.piece.col+vc;
_44.push([nr,nc]);
}
return _44;
};
}
function CannonMoveGenerator(_4c){
this.board=_4c.board.board;
this.piece=_4c;
this.velocity=[[-1,0],[1,0],[0,-1],[0,1]];
this.searchDirection=function(vr,vc){
var _4f=[];
var nr=this.piece.row;
var nc=this.piece.col;
while(true){
nr+=vr;
nc+=vc;
if(nr<0||nr>=MAX_ROW||nc<0||nc>=MAX_COL){
break;
}
if(this.board[nr][nc].type!=EMPTY){
while(true){
nr+=vr;
nc+=vc;
if(nr<0||nr>=MAX_ROW||nc<0||nc>=MAX_COL){
break;
}
if(this.board[nr][nc].type==EMPTY){
continue;
}
if(this.board[nr][nc].side!=this.piece.side){
_4f.push([nr,nc]);
break;
}
}
break;
}else{
_4f.push([nr,nc]);
}
}
return _4f;
};
this.generateMoves=function(){
var _52=[];
for(var i=0;i<this.velocity.length;i++){
var vr=this.velocity[i][0];
var vc=this.velocity[i][1];
var m=this.searchDirection(vr,vc);
_52=_52.concat(m);
}
return _52;
};
}
function Piece(_57,row,col,_5a,_5b){
this.board=_57;
this.row=row;
this.col=col;
this.side=_5b;
this.type=_5a;
this.name="empty";
this.nextMoves=[];
if(this.type<UNIT_TYPE.length){
this.name=UNIT_TYPE[this.type];
}
this.canTakeOver=function(_5c){
return (_5c.type==EMPTY)||(this.side!=_5c.side);
};
this.generateNextMoves=function(){
this.nextMoves=generateMoves(this);
};
}
function Board(){
this.board=new Array(MAX_ROW);
for(var i=0;i<MAX_ROW;i++){
this.board[i]=new Array(MAX_COL);
}
this.initialize=function(_5e){
var _5f=_5e.board;
for(var row=0;row<MAX_ROW;row++){
for(var col=0;col<MAX_COL;col++){
var _62=_5f[row][col][0];
var _63=_5f[row][col][1];
this.board[row][col]=new Piece(this,row,col,_62,_63);
}
}
};
this.movePiece=function(_64,to){
if(_64.type==EMPTY){
return;
}
var tmp=_64;
this.board[_64.row][_64.col]=new Piece(this,_64.row,_64.col,EMPTY,0);
this.board[to.row][to.col]=new Piece(this,to.row,to.col,tmp.type,tmp.side);
};
}
function ChineseChessView(_67){
this.boardId=_67;
this.container=document.createElement("div");
this.container.className="board";
this.container.id=this.boardId;
this.initialize=function(_68){
for(var row=0;row<MAX_ROW;row++){
var _6a="boardRow boardRow"+row;
for(var col=0;col<MAX_COL;col++){
var _6c=_6a+" boardColumn boardColumn"+col;
var _6d=_6c+" boardCell boardCell"+row+col;
ee=document.createElement("div");
ee.className=_6d;
var pe=document.createElement("div");
pe.id=generatePieceId(this.boardId,row,col);
ee.appendChild(pe);
this.container.appendChild(ee);
}
}
var _6f=document.createElement("div");
_6f.className="redStatus";
_6f.id=this.boardId+"_red";
this.container.appendChild(_6f);
var _70=document.createElement("div");
_70.className="blackStatus";
_70.id=this.boardId+"_black";
this.container.appendChild(_70);
};
this.generateClassName=function(_71){
var _72="";
if(_71.type!=EMPTY){
if(_71.side==RED_SIDE){
_72="red_";
}else{
_72="black_";
}
}
_72+=_71.name;
return _72;
};
this.update=function(_73,_74,_75){
for(var row=0;row<MAX_ROW;row++){
for(var col=0;col<MAX_COL;col++){
var _78=generatePieceId(this.boardId,row,col);
var div=document.getElementById(_78);
div.piece=_73[row][col];
div.className=this.generateClassName(_73[row][col]);
}
}
removeChildren(document.getElementById(this.boardId+"_red"));
removeChildren(document.getElementById(this.boardId+"_black"));
var _7a=document.createElement("div");
_7a.className="turn";
var div=null;
if(_74==RED_SIDE){
div=document.getElementById(this.boardId+"_red");
}else{
div=document.getElementById(this.boardId+"_black");
}
div.appendChild(_7a);
if(!_75){
return;
}
if((_75&RED_SIDE)!=0){
var win=document.createElement("div");
win.className="winner";
div=document.getElementById(this.boardId+"_red");
div.appendChild(win);
}
if((_75&BLACK_SIDE)!=0){
var win=document.createElement("div");
win.className="winner";
div=document.getElementById(this.boardId+"_black");
div.appendChild(win);
}
};
this.mark=function(_7c){
for(var i=0;i<_7c.length;i++){
var loc=_7c[i];
var _7f=generatePieceId(this.boardId,loc[0],loc[1]);
var div=document.getElementById(_7f);
div.style.backgroundColor="blue";
div.style.opacity="0.4";
div.style.cursor="pointer";
}
};
this.demark=function(_81){
for(var i=0;i<_81.length;i++){
var loc=_81[i];
var _84=generatePieceId(this.boardId,loc[0],loc[1]);
var div=document.getElementById(_84);
div.style.backgroundColor="";
div.style.opacity="";
div.style.cursor="";
}
};
}
function MatchConfiguration(_86,_87){
this.visitBoardConfiguration=function(_88){
var _89={"t":TANK,"h":HORSE,"e":ELEPHANT,"g":GUARD,"k":KING,"c":CANNON,"p":PAWN};
var _8a=_88.getElementsByTagName("div");
this.board=new Array(MAX_ROW);
for(var row=0;row<MAX_ROW;row++){
var _8c=_8a[row].firstChild.nodeValue;
this.board[row]=new Array(MAX_COL);
for(var col=0;col<MAX_COL;col++){
var c=_8c.charAt(col);
var _8f=RED_SIDE;
var _90=EMPTY;
if("A"<=c&&c<="Z"){
_8f=BLACK_SIDE;
c=c.toLowerCase();
}
if(c in _89){
_90=_89[c];
}
this.board[row][col]=[_90,_8f];
}
}
};
var _91=_86.getElementsByTagName("div");
for(var i=0;i<_91.length;i++){
var div=_91[i];
if(div.className=="boardConfiguration"){
this.visitBoardConfiguration(div);
}
}
}
function HistoryView(){
this.container=document.createElement("div");
this.container.className="history";
this.content=document.createElement("div");
this.content.className="historyContent";
this.control=document.createElement("div");
this.control.className="historyControl";
this.container.appendChild(this.control);
this.container.appendChild(this.content);
this.backButton=document.createElement("span");
this.backButton.className="historyBack";
this.backButton.innerHTML="&lt;---";
this.forwardButton=document.createElement("span");
this.forwardButton.className="historyForward";
this.forwardButton.innerHTML="---&gt;";
this.control.appendChild(this.backButton);
this.control.appendChild(this.forwardButton);
this.update=function(_94){
var _95=["a","b","c","d","e","f","g","h","i"];
removeChildren(this.content);
for(var i=_94.length;i>0;i--){
var _97=_94[i-1];
var li=document.createElement("div");
li.innerHTML=i+"&mdash;"+_95[_97[1]]+(MAX_ROW-_97[0])+" "+_95[_97[3]]+(MAX_ROW-_97[2]);
this.content.appendChild(li);
}
};
}
function ChineseChessController(_99,_9a){
this.board=new Board();
this.config=new MatchConfiguration(_99,_9a);
this.boardId=generateBoardId();
this.view=new ChineseChessView(this.boardId);
this.historyView=new HistoryView();
this.element=_99;
this.nextPlayer=RED_SIDE;
this.selectedPiece=null;
this.moves=[];
this.lastUpdateTime=0;
this.updateTimer=null;
this.serverUrl=null;
this.historyLevel=0;
this.side=_9a;
this.winner=0;
this.start=function(_9b){
this.serverUrl=_9b;
this.view.initialize(this.config);
this.board.initialize(this.config);
removeChildren(this.element);
this.element.appendChild(this.historyView.container);
this.element.appendChild(this.view.container);
this.view.update(this.board.board,this.nextPlayer,0);
this.installHistoryListener();
this.installMouseListener();
this.installRefreshListener();
};
this.installHistoryListener=function(){
this.historyView.backButton.controller=this;
this.historyView.backButton.onclick=this.handleHistoryBack;
this.historyView.forwardButton.controller=this;
this.historyView.forwardButton.onclick=this.handleHistoryForward;
};
this.buildBoard=function(_9c){
var _9d=new Board();
_9d.initialize(me.config);
for(var i=0;i<_9c;i++){
var row=me.moves[i][0];
var col=me.moves[i][1];
var _a1=_9d.board[row][col];
row=me.moves[i][2];
col=me.moves[i][3];
var to=_9d.board[row][col];
_9d.movePiece(_a1,to);
}
return _9d;
};
this.handleHistoryBack=function(){
me=this.controller;
if(me.historyLevel<=0){
return;
}
me.historyLevel--;
var _a3=me.buildBoard(me.historyLevel);
me.view.update(_a3.board,me.nextPlayer,me.winner);
};
this.handleHistoryForward=function(){
me=this.controller;
if(me.historyLevel>=me.moves.length){
return;
}
me.historyLevel++;
var _a4=me.buildBoard(me.historyLevel);
me.view.update(_a4.board,me.nextPlayer,me.winner);
};
this.sendUpdate=function(){
var _a5=new XHR({method:"post",onSuccess:this.sendUpdateSuccess,onFailure:this.sendUpdateFailure,isSuccess:function(_a6){
return _a6==0||(_a6>=200&&_a6<300);
}});
_a5.controller=this;
if(!LOCAL_MODE){
_a5.send(this.serverUrl,encodeUri({"action":"update","index":this.moves.length}));
}else{
_a5.send(this.serverUrl+this.moves.length);
}
};
this.sendUpdateSuccess=function(_a7,xml){
controller=this.controller;
var _a9=_a7.split(/\r?\n/);
var _aa=new Number(_a9.shift()).valueOf();
for(var i=_aa;i<controller.moves.length;i++){
_a9.shift();
}
for(var i=0;i<_a9.length;i++){
var _ac=_a9[i];
if("M"==_ac.charAt(0)){
var _ad=[];
for(var j=0;j<4;j++){
_ad[j]=(new Number(_ac.charAt(j+1))).valueOf();
}
controller.executeMove(_ad);
}else{
if("E"==_ac.charAt(0)){
controller.winner=(new Number(_ac.charAt(1))).valueOf();
controller.gameOver(_ac);
}
}
}
};
this.gameOver=function(_af){
this.side=0;
this.view.update(this.board.board,this.nextPlayer,this.winner);
$clear(this.updateTimer);
};
this.sendUpdateFailure=function(){
};
this.handleTimer=function(_b0){
var now=(new Date()).getTime();
if(now-this.lastUpdateTime<_b0){
return;
}
this.sendUpdate();
this.lastUpdateTime=(new Date()).getTime();
};
this.installRefreshListener=function(){
this.lastUpdateTime=(new Date()).getTime();
this.updateTimer=this.handleTimer.periodical(5000,this,5000);
};
this.installMouseListener=function(){
for(var row=0;row<MAX_ROW;row++){
for(var col=0;col<MAX_COL;col++){
var _b4=generatePieceId(this.boardId,row,col);
var div=document.getElementById(_b4);
div.onclick=this.handleClick;
div.controller=this;
}
}
};
this.sendMove=function(_b6,_b7){
var _b8=new XHR({method:"post",onSuccess:this.sendMoveSuccess,onFailure:this.sendMoveFailure,isSuccess:function(_b9){
return _b9==0||(_b9>=200&&_b9<300);
}});
_b8.controller=this;
if(!LOCAL_MODE){
_b8.send(this.serverUrl,encodeUri({"action":"move","row1":_b7[0],"col1":_b7[1],"row2":_b7[2],"col2":_b7[3]}));
}else{
_b8.send(this.serverUrl+_b6);
}
};
this.executeMove=function(_ba){
try{
soundManager.play(this.boardId+"_sound","/sound/wood.mp3");
}
catch(err){
}
this.moves.push(_ba);
var row=_ba[0];
var col=_ba[1];
var _bd=this.board.board[row][col];
row=_ba[2];
col=_ba[3];
var to=this.board.board[row][col];
this.board.movePiece(_bd,to);
if(this.nextPlayer==RED_SIDE){
this.nextPlayer=BLACK_SIDE;
}else{
this.nextPlayer=RED_SIDE;
}
this.view.update(this.board.board,this.nextPlayer,this.winner);
this.historyView.update(this.moves);
this.historyLevel=this.moves.length;
};
this.sendMoveSuccess=function(_bf,xml){
var _c1=this.controller;
_c1.executeMove(_c1.pendingMove);
};
this.sendMoveFailure=function(_c2){
};
this.handleSecondClick=function(_c3){
if(this.nextPlayer!=this.side||this.historyLevel!=this.moves.length){
return;
}
var _c4=this.selectedPiece.nextMoves;
for(var i=0;i<_c4.length;i++){
var row=_c4[i][0];
var col=_c4[i][1];
if(_c3.row==row&&_c3.col==col){
this.pendingMove=[this.selectedPiece.row,this.selectedPiece.col,_c3.row,_c3.col];
this.sendMove(this.moves.length,this.pendingMove);
}
}
this.view.demark(_c4);
this.selectedPiece=null;
};
this.handleFirstClick=function(_c8){
if(this.nextPlayer!=this.side||this.historyLevel!=this.moves.length){
return;
}
_c8.generateNextMoves();
if(_c8.nextMoves.length==0){
return;
}
this.selectedPiece=_c8;
this.view.mark(_c8.nextMoves);
};
this.handleClick=function(evt){
var _ca=this.piece;
var _cb=this.controller;
if(_cb.selectedPiece!=null){
_cb.handleSecondClick(_ca);
}else{
if(_ca.type==EMPTY||_cb.side!=_ca.side){
return;
}else{
_cb.handleFirstClick(_ca);
}
}
};
}
function initBoard(_cc,_cd){
var _ce=Array();
function initElement(_cf){
if("chineseChess"==_cf.className){
_ce.push(_cf);
}else{
if(_cf.hasChildNodes()){
for(var i=0;i<_cf.childNodes.length;i++){
initElement(_cf.childNodes[i]);
}
}
}
}
initElement(document.documentElement);
while(_ce.length){
var _d1=_ce.pop();
var _d2=new ChineseChessController(_d1,_cc);
if(_cd==null){
_cd=SERVER_URL;
}
_d2.start(_cd);
}
}
function sendDraw(_d3){
var _d4=new XHR({method:"post",onSuccess:function(){
},onFailure:function(){
},isSuccess:function(_d5){
return _d5==0||(_d5>=200&&_d5<300);
}});
_d4.send(_d3,encodeUri({"action":"draw"}));
}
function sendResign(_d6){
var _d7=new XHR({method:"post",onSuccess:function(){
},onFailure:function(){
},isSuccess:function(_d8){
return _d8==0||(_d8>=200&&_d8<300);
}});
_d7.send(_d6,encodeUri({"action":"resign"}));
}

