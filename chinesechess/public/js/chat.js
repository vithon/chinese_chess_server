LOCAL_MODE=document.location.protocol=="file:";
SERVER_URL="server.html";
VK_UP=38;
VK_DOWN=40;
VK_RETURN=13;
function encodeUri(_1){
var _2=[];
for(key in _1){
_2.push(escape(key)+"="+encodeURIComponent(_1[key]));
}
return _2.join("&");
}
function ChatView(_3){
this.element=_3;
this.initialize=function(){
this.chatArea=document.createElement("div");
this.chatArea.className="chatMessageDisplay";
this.element.appendChild(this.chatArea);
};
this.appendMessage=function(_4,_5,_6){
var _7=document.createElement("div");
_7.className="chatMessageTopLeft";
var _8=document.createElement("div");
_8.className="chatMessageTopRight";
var _9=document.createElement("div");
_9.className=_6;
var _a=document.createElement("div");
_a.className="chatHeaderLeft";
var _b=document.createElement("div");
_b.className="chatHeaderRight";
var _c=document.createElement("div");
_c.className="chatAuthor";
var _d=document.createTextNode(_4);
_c.appendChild(_d);
_7.appendChild(_8);
_8.appendChild(_9);
_9.appendChild(_a);
_a.appendChild(_b);
_b.appendChild(_c);
var _e=document.createElement("div");
_e.className="chatMessageBottomLeft";
var _f=document.createElement("div");
_f.className="chatMessageBottomRight";
var _10=document.createElement("div");
_10.className="chatMessage";
var _11=document.createTextNode(_5);
_10.appendChild(_11);
_8.appendChild(_e);
_e.appendChild(_f);
_f.appendChild(_10);
this.chatArea.appendChild(_7);
this.chatArea.scrollTop=this.chatArea.scrollHeight;
};
this.appendSelf=function(_12,_13){
this.appendMessage(_12,_13,"chatHeaderSelf");
};
this.appendOther=function(_14,_15){
this.appendMessage(_14,_15,"chatHeaderOther");
};
}
function ChatInput(_16){
this.element=_16;
this.listeners=[];
this.initialize=function(){
var _17=document.createElement("form");
_17.className="chatMessageForm";
this.input=document.createElement("input");
this.input.type="text";
this.input.className="chatMessageInput";
this.input.myself=this;
this.input.onkeydown=this.onKeyDown;
_17.appendChild(this.input);
this.element.appendChild(this.input);
};
this.addKeyListener=function(_18){
this.listeners.push(_18);
};
this.onKeyDown=function(evt){
if(evt.target==this){
for(var i=this.myself.listeners.length-1;i>=0;i--){
var ret=this.myself.listeners[i](evt,this.myself);
if(ret==false){
return false;
}
}
}
return true;
};
this.getValue=function(){
return this.input.value;
};
this.setValue=function(msg){
this.input.value=msg;
};
}
function ChatController(_1d,_1e){
this.view=new ChatView(_1d);
this.input=new ChatInput(_1e);
this.serverUrl=null;
this.messages=[];
this.currentIndex=0;
this.messageCount=0;
this.refreshTime=null;
this.author=0;
this.start=function(_1f,_20){
this.author=_1f;
this.serverUrl=_20||SERVER_URL;
this.view.initialize();
this.input.initialize();
this.input.controller=this;
this.input.addKeyListener(this.onKeyDown);
this.installRefreshListener();
};
this.installRefreshListener=function(){
this.refreshTime=(new Date()).getTime();
this.refresh.periodical(2000,this,2000);
};
this.refresh=function(_21){
var now=(new Date()).getTime();
if(now<this.refreshTime+_21){
return;
}
this.refreshTime=now;
var _23=new XHR({onSuccess:this.refreshSuccess,isSuccess:function(_24){
return _24==0||(_24>=200&&_24<300);
}});
_23.controller=this;
if(!LOCAL_MODE){
_23.send(this.serverUrl,encodeUri({"action":"update"}));
}else{
_23.send(this.serverUrl+this.messageCount);
}
};
this.refreshSuccess=function(_25,xml){
var me=this.controller;
var _28=_25.split(/\r?\n/);
while(_28.length>2){
var id=_28.shift();
id=(new Number(id)).valueOf();
var _2a=_28.shift();
var msg=_28.shift();
if(msg==undefined){
break;
}
if(id<=me.messageCount){
continue;
}
if(_2a==me.author){
me.view.appendSelf(_2a,msg);
}else{
me.view.appendOther(_2a,msg);
}
me.messageCount=id;
}
};
this.onUp=function(src){
this.currentIndex--;
if(this.currentIndex<0){
this.currentIndex=0;
}
var _2d="";
if(this.currentIndex<this.messages.length){
_2d=this.messages[this.currentIndex];
}
this.input.setValue(_2d);
};
this.onDown=function(src){
this.currentIndex++;
var _2f="";
if(this.currentIndex<this.messages.length){
_2f=this.messages[this.currentIndex];
}else{
this.currentIndex=this.messages.length;
}
this.input.setValue(_2f);
};
this.sendMessage=function(_30){
var _31=new XHR({isSuccess:function(_32){
return _32==0||(_32>=200&&_32<300);
}});
if(!LOCAL_MODE){
var _33=encodeUri({"action":"send","message":_30});
_31.send(this.serverUrl,_33);
}else{
_31.send(this.serverUrl+this.messageCount);
}
};
this.onEnter=function(src){
var _35=this.input.getValue();
if(this.messages.length>100){
this.messages.shift();
}
this.messages.push(_35);
this.currentIndex=this.messages.length;
this.sendMessage(_35);
this.input.setValue("");
};
this.onKeyDown=function(evt,src){
if(evt.keyCode!=VK_UP&&evt.keyCode!=VK_DOWN&&evt.keyCode!=VK_RETURN){
return true;
}
if(evt.keyCode==VK_UP){
src.controller.onUp(src);
return false;
}else{
if(evt.keyCode==VK_DOWN){
src.controller.onDown(src);
return false;
}
}
src.controller.onEnter(src);
return false;
};
}

