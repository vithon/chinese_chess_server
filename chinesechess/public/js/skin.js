MAIN_SKIN = 'rowcol';

BOARD_ORIENTATION = 'vboard';

function loadSkin() {
	var mainSkin = readCookie('skinChineseChess');
	if (mainSkin != null) {
		MAIN_SKIN = mainSkin;
	}
	var orientation = readCookie('skinBoardOrientation');
	if (orientation != null) {
		BOARD_ORIENTATION = orientation;
	}
	applyCss();
}

function changeSkin(skin) {
	MAIN_SKIN = skin;
	applyCss();
}

function switchSide() {
	if (BOARD_ORIENTATION == 'vboard') {
		BOARD_ORIENTATION = 'rboard';
	} else {
		BOARD_ORIENTATION = 'vboard';
	}
	applyCss();
}

function applyCss() {
	var skin = document.getElementById('skinChineseChess');
	skin.href = '/skins/' + MAIN_SKIN + '/' + 'skin.css';
	skin = document.getElementById('skinBoardOrientation');
	skin.href = '/skins/' + MAIN_SKIN + '/' + BOARD_ORIENTATION + '.css';
	createCookie('skinChineseChess', MAIN_SKIN, 30);
	createCookie('skinBoardOrientation', BOARD_ORIENTATION, 30);
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}
